/**
 *	A really simple method to help set, read, and update values in
 *  the current window's hash
 *	@constructor
 */
var HashManager = function(options){
    // The frequency of which to check for a hash change
    this.__checkInterval = 500;

    // The default method to run when a change has been detected
    this.__onChange = function(){};

    // Were we passed an onChange option?
    if( options['onChange'] != undefined ) {
        this.__onChange = options['onChange'];
        this.__startMonitor();
    }

    // Were we passed a checkInterval?
    if( options['checkInterval'] != undefined )
        this.__checkInterval = options['checkInterval'];
};

/**
 *	Reads the current hash and returns the fields and values
 *  as an object.
 *
 *	@param {string} field - The name of the field if you only want to
 *      return a single field's value.
 */
HashManager.prototype.readHash = function(field){
    // Will hold our hash fields and values
    var hashObject = {};

    // Getting our current hash and splitting it into pieces
    var currHash = window.location.hash.substr(1);
    var pieces = currHash.split('&');

    // Loop each piece and split up by field and value
    for( var x in pieces ){
        var sections = pieces[x].split('=');
        hashObject[sections[0]] = sections[1];
    }

    // No field was provided, so just return the whole hash object
    if( field == undefined ){
        return hashObject;
    }

    // A field was passed, but doesn't exist in the object
    if( hashObject[field] == undefined ){
        throw "Hash object doesn't container \""+ field +"\"";
        return;
    }

    // Returning the value of the given field
    return hashObject[field];
};

/**
 *  Sets the hash to the passed objects
 *
 *	@param {object} hashVals - The list of fields and values to
 *      give to the hash
 */
HashManager.prototype.setHash = function(hashVals){
    var theHash = "";
    var tempVals = [];

    for( var x in hashVals ) {
        var theField = x;
        var theValue = hashVals[x];
        tempVals.push(x + "=" + theValue);
    }

    theHash = tempVals.join("&");
    window.location.hash = theHash;
};

/**
 *	Begins the loop to check for changes in the hash
 */
HashManager.prototype.__startMonitor = function(){
    // Getting our current hash as a string
    var currHash = window.location.hash.substr(1);

    // Performs the actual check
    var doCheck = function(){
        // Get our current hash for this interval
        var thisHash = window.location.hash.substr(1);

        // There was a change
        if( thisHash != currHash ){
            this.__triggerChange();
            currHash = thisHash;
        }

        // Run the check again in X amount of time
        setTimeout(doCheck, this.__checkInterval);
    }.bind(this);

    doCheck();
};

// Is run whenever a change was detected in the hash
HashManager.prototype.__triggerChange = function(){
    var currHash = this.readHash();
    this.__onChange(currHash);
};


module.exports = HashManager;
