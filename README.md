# Hash Manager

A really simple way to set, read, and update the hash.

## Disclaimer

This is a work in progress and should be used with caution.

## Usage

### Include in Project

First, include it in your project:

    var HashManager = require('hashmanager');

Now instantiate:

    var hm = new HashManager();

### Setting the Hash

Set your hash to a list of values:

    hm.setHash({
        field1: 'value1',
        field2: 'value2'
    })

This will set the hash to `example.com#field1=value1&field2=value2`.

### Reading the hash

On a page with a URL of `example.com#field1=value1&field2=value2`:

    hm.readHash();

Will return the value:

    {
        field1: 'value1',
        field2: 'value2'
    }

### Read Single Field

    hm.readHash('field1');

Will return a value of `value1`.